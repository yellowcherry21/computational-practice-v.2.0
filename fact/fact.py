def amount_of_digits(m: int) -> int:
    i: int = 0
    factorial: int = 1
    while m > 1:
        factorial *= m
        m -= 1

    while factorial > 0:
        factorial //= 10
        i = i + 1
    return i


assert amount_of_digits(0) == 1
assert amount_of_digits(1) == 1
assert amount_of_digits(2) == 1
assert amount_of_digits(3) == 1
assert amount_of_digits(4) == 2
assert amount_of_digits(5) == 3
assert amount_of_digits(6) == 3
assert amount_of_digits(7) == 4
assert amount_of_digits(8) == 5
assert amount_of_digits(9) == 6
assert amount_of_digits(10) == 7
assert amount_of_digits(11) == 8
assert amount_of_digits(12) == 9
assert amount_of_digits(13) == 10
assert amount_of_digits(14) == 11
assert amount_of_digits(15) == 13
assert amount_of_digits(16) == 14
assert amount_of_digits(17) == 15
assert amount_of_digits(18) == 16
assert amount_of_digits(19) == 18
assert amount_of_digits(25) == 26
assert amount_of_digits(50) == 65
assert amount_of_digits(75) == 110
assert amount_of_digits(100) == 158
assert amount_of_digits(125) == 210
assert amount_of_digits(150) == 263
assert amount_of_digits(175) == 319
assert amount_of_digits(200) == 375
assert amount_of_digits(225) == 434
assert amount_of_digits(250) == 493
assert amount_of_digits(600) == 1409
assert amount_of_digits(625) == 1478
assert amount_of_digits(650) == 1548
assert amount_of_digits(675) == 1619
assert amount_of_digits(700) == 1690
assert amount_of_digits(725) == 1761
assert amount_of_digits(750) == 1833
assert amount_of_digits(775) == 1905
assert amount_of_digits(800) == 1977
assert amount_of_digits(825) == 2050
assert amount_of_digits(850) == 2123
assert amount_of_digits(875) == 2197
assert amount_of_digits(900) == 2270
assert amount_of_digits(925) == 2344
assert amount_of_digits(950) == 2419
assert amount_of_digits(975) == 2493